import React from 'react';
import { StyleSheet, Text, View, ImageBackground } from 'react-native';
import Planet from './src/components/Planet';
import image from './src/images/wallpaper.jpg'

export default function App() {
  return (
    <View style={styles.container}>
      
        <Planet />
      
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'black',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
