import React, { Component } from 'react'
import { View, StyleSheet, Text  } from 'react-native'
import { Button, Icon } from 'react-native-elements'
import { getPlanet } from '../services/api'

export default class Planet extends Component {

    componentDidMount = () => {
        this.nextPlanet()
    }

    state = {
        planet: {}
    }

    nextPlanet = () => {
        getPlanet().then(resp => {
            console.log(resp)
            this.setState({ planet: resp.data })
        })
    }

    render() {
        const { planet } = this.state

        return (
            <View style={styles.container}>
                <View style={{}}>
                    <Text style={styles.title}>Planet's Star Wars</Text>

                    <View style={styles.info}>
                        <Icon type={'font-awesome'} name={'globe'} size={25} 
                        color={'#fff'} />
                        <Text style={styles.text}>{planet.name}</Text>
                    </View>

                    <View style={styles.info}>
                        <Icon type={'font-awesome'} name={'users'} size={25} 
                        color={'#fff'} />
                        <Text style={styles.text}>{planet.population}</Text>
                    </View>

                    <View style={styles.info}>
                        <Icon type={'font-awesome'} name={'bolt'} size={25} 
                        color={'#fff'} />
                        <Text style={styles.text}>{planet.climate}</Text>
                    </View>

                    <View style={styles.info}>
                        <Icon type={'font-awesome'} name={'info-circle'} size={25} 
                        color={'#fff'} />
                        <Text style={styles.text}>{planet.terrain}</Text>
                    </View>

                </View>

                <Button title={'Next'} onPress={() => this.nextPlanet()} 
                buttonStyle={{ backgroundColor: 'red', }} />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    infoArea: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#123456',
        width: '90%',
        height: '50%',
        borderColor: 'black'
    },
    info: {
        flexDirection: 'row', 
        alignItems: 'center', 
    },
    title: {
        fontSize: '24px',
        color: 'white',
        marginBottom: 20,
    },
    text: {
        color: '#fff',
        margin: 10,
        marginLeft: 20,
    }
})
