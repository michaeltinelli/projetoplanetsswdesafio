import axios from 'axios'
const baseUrl = 'https://swapi.co/api/planets'

export function getPlanet() {
    const min = 1
    const max = 61
    const result = Math.floor( Math.random() * (max - min) + min )
    console.log(result)
    return axios.get(`${baseUrl}/${result}`)
}